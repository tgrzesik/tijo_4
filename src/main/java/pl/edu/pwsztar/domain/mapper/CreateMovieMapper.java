package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class CreateMovieMapper {

    public Movie mapToEntity(CreateMovieDto dto) {
        Movie move = new Movie();

        move.setTitle(dto.getTitle());
        move.setImage(dto.getImage());
        move.setYear(dto.getYear());

        return move;
    }
}
